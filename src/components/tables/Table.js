import React from 'react'
import {
  CCard,
  CCardBody,
  CCardHeader,
  CCol,
  CRow,
  CTable,
  CTableBody,
  CTableCaption,
  CTableDataCell,
  CTableHead,
  CTableHeaderCell,
  CTableRow,
} from '@coreui/react'
import { DocsExample } from '../DocsExample.js'

const Tables = () => {
  return (
      <CCol>
        <CCard className="mb-4">
            <DocsExample href="components/table#striped-rows">
              <CTable striped>
                <CTableHead>
                  <CTableRow>
                    <CTableHeaderCell scope="col">#</CTableHeaderCell>
                    <CTableHeaderCell scope="col">Tên hoạt động</CTableHeaderCell>
                    <CTableHeaderCell scope="col">Thể loại</CTableHeaderCell>
                    <CTableHeaderCell scope="col">Thời gian tổ chức</CTableHeaderCell>
                    <CTableHeaderCell scope="col">Thao tác</CTableHeaderCell>
                  </CTableRow>
                </CTableHead>
                <CTableBody>
                  <CTableRow>
                    <CTableHeaderCell scope="row">1</CTableHeaderCell>
                    <CTableDataCell>Giải thể thao Công Đoàn</CTableDataCell>
                    <CTableDataCell>Văn - Thể - Mỹ</CTableDataCell>
                    <CTableDataCell>24/01/2024 -  24/01/2024</CTableDataCell>
                    <CTableDataCell></CTableDataCell>
                  </CTableRow>
                  <CTableRow>
                    <CTableHeaderCell scope="row">2</CTableHeaderCell>
                    <CTableDataCell>Giải thể thao Công Đoàn</CTableDataCell>
                    <CTableDataCell>Văn - Thể - Mỹ</CTableDataCell>
                    <CTableDataCell>24/01/2024 -  24/01/2024</CTableDataCell>
                    <CTableDataCell></CTableDataCell>
                  </CTableRow>
                  <CTableRow>
                    <CTableHeaderCell scope="row">3</CTableHeaderCell>
                    <CTableDataCell>Giải thể thao Công Đoàn</CTableDataCell>
                    <CTableDataCell>Văn - Thể - Mỹ</CTableDataCell>
                    <CTableDataCell>24/01/2024 -  24/01/2024</CTableDataCell>
                    <CTableDataCell></CTableDataCell>
                  </CTableRow>
                </CTableBody>
              </CTable>
            </DocsExample>
        </CCard>
      </CCol>
  )
}

export default Tables
