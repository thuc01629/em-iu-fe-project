import { DocsExample } from '../DocsExample.js';
import { CButton, CCard, CCardBody, CCardHeader, CCol, CRow } from '@coreui/react';
import React from 'react'

const Button = () => {
    return (
        <CCol>
            <DocsExample href="components/buttons">
              {['normal'].map((state, index) => (
                <CRow className="align-items-center mb-3" key={index}>
                  <CCol xs>
                    {[
                      'info',
                    ].map((color, index) => (
                      <CButton
                        color={color}
                        key={index}
                        active={state === 'active'}
                        disabled={state === 'disabled'}
                      >
                        Tạo mới
                      </CButton>
                    ))}
                  </CCol>
                </CRow>
              ))}
            </DocsExample>
      </CCol>
    )
}

export default Button

