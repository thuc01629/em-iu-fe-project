
import {
    CForm,
    CFormInput,
    CFormLabel,
    CFormTextarea,
    CFormSelect
  } from '@coreui/react'
import { DocsExample } from '../DocsExample.js';

const RegisterForm = () => {
    return (
        <DocsExample href="forms/form-control">
            <CForm>
            <div className="mb-3">
                <CFormLabel>Tên hoạt động</CFormLabel>
                <CFormInput
                id="activityName"
                placeholder="Tên hoạt động"
                />
            </div>
            <div className="mb-3">
                <CFormLabel>Thể loại hoạt động</CFormLabel>
                <CFormSelect aria-label="Default select example">
                    {/* <option>Thể loại hoạt động</option> */}
                    <option value="1">One</option>
                    <option value="2">Two</option>
                    <option value="3">Three</option>
                </CFormSelect>
            </div>
            <div>
                <CFormLabel>Ngày tổ chức</CFormLabel>
            </div>
            </CForm>
        </DocsExample>
    )
}

export default RegisterForm