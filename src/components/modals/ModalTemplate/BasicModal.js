import {
    CButton,
    CModal,
    CModalBody,
    CModalFooter,
    CModalHeader,
    CModalTitle,
  } from '@coreui/react'
  import { DocsExample } from '../../DocsExample.js';

const BasicModal = (props) => {
    const { children } = props
    return (
        <DocsExample href="components/modal">
            <CModal
            className="show d-block position-static"
            backdrop={false}
            keyboard={false}
            portal={false}
            visible
            >
            <CModalHeader>
                <CModalTitle>Thêm mới hoạt động</CModalTitle>
            </CModalHeader>
            <CModalBody>{children}</CModalBody>
            <CModalFooter>
                <CButton color="secondary">Close</CButton>
                <CButton color="primary">Save changes</CButton>
            </CModalFooter>
            </CModal>
        </DocsExample>
    )
}

export default BasicModal