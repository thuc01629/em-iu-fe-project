import BasicModal from './ModalTemplate/BasicModal.js'
import RegisterForm from '../forms/RegisterForm.js'

const RegisterModal = () => {
    return (
        <BasicModal>
            <RegisterForm/>
        </BasicModal>
    )
}

export default RegisterModal