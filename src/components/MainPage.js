import Tables from './tables/Table.js';
import Button from './buttons/Button.js';
import RegisterModal from './modals/RegisterModal.js';

const MainPage = () => {
    return (
        <>
            <Button/>
            <Tables/>
            <RegisterModal/>
        </>
        
    )
}

export default MainPage