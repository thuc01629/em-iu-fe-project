// import logo from './logo.svg';
import MainPage from './components/MainPage.js'
import './scss/style.scss'
import React from 'react'
import { HashRouter, Route, Routes } from 'react-router-dom'
// import routes from '../routes'

function App() {
  return (
    <HashRouter>
      <Routes>
        <Route exact path="*" name="Main Page" element={<MainPage/>} />
      </Routes>
  </HashRouter>
  );
}

export default App;
